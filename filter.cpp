
/* includes */

#include <streams.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include "filter.h"


/* defines */

// Define the MPEG-TS packet size in bytes
#define FILTER_PACKET_SIZE      188
// Defines the maximum number of sample buffers we need
#define FILTER_SAMPLE_BUFFERS   100
// Defines the maximum number of packets in a sample
#define FILTER_MAXIMUM_PACKETS  1000
// Defines the total sample size
#define FILTER_SAMPLE_SIZE      (FILTER_PACKET_SIZE * FILTER_MAXIMUM_PACKETS)


/* locals */

// Pin type setup structure
const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_Stream,      // Major type
    &MEDIASUBTYPE_NULL      // Minor type
};

// Input/Output pins setup structure
const AMOVIESETUP_PIN sudpPins = 
{
    L"Output",              // Obsolete, not used.
    FALSE,                  // Is this pin rendered?
    TRUE,                   // Is it an output pin?
    FALSE,                  // Can the filter create zero instances?
    FALSE,                  // Does the filter create multiple instances?
    &CLSID_NULL,            // Obsolete.
    NULL,                   // Obsolete.
    1,                      // Number of media types.
    &sudPinTypes            // Pointer to media types.
};

// Filter setup structure
const AMOVIESETUP_FILTER sudFilter =
{
    &CLSID_TSPushFilter,    // Filter CLSID
    FILTER_NAME,            // String name
    MERIT_DO_NOT_USE,       // Filter merit
    1,                      // Number pins
    &sudpPins               // Pin details
};

// COM declaration of each filter supported by the DLL
static WCHAR g_wszName[] = FILTER_NAME;

// Filter template
CFactoryTemplate g_Templates[] = 
{
    { 
        g_wszName,              // Name
        &CLSID_TSPushFilter,    // CLSID
        Filter::CreateInstance, // Method to create an instance of MyComponent
        NULL,                   // Initialization function
        &sudFilter              // Set-up information (for filters)
    }
};

// Total number of filters
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);    


/* implementation */

// Setup the MPEG-TS media type
HRESULT Pin::GetMediaType(CMediaType * pMediaType)
{
    pMediaType->SetType(&MEDIATYPE_Stream);
    pMediaType->SetSubtype(&MEDIASUBTYPE_MPEG2_TRANSPORT);
    pMediaType->SetTemporalCompression(FALSE);
    pMediaType->SetSampleSize(FILTER_SAMPLE_SIZE);
    return S_OK;
}

// Setup the buffer size
HRESULT Pin::DecideBufferSize(IMemAllocator * pAlloc, ALLOCATOR_PROPERTIES * pRequest)
{
    HRESULT hResult;
    ALLOCATOR_PROPERTIES actual;

    // Sets alignment
    pRequest->cbAlign = true;
    // Decide how many output sample buffers we need
    pRequest->cBuffers = FILTER_SAMPLE_BUFFERS;
    // Decide the maximum size of a sample
    pRequest->cbBuffer = (FILTER_SAMPLE_SIZE);
    // Set output allocator properties
    if (FAILED(hResult = pAlloc->SetProperties(pRequest, &actual))) 
    {
        return hResult;
    }
    // Double check we actually got what requested
    if ((pRequest->cBuffers != actual.cBuffers) || (pRequest->cbBuffer != actual.cbBuffer)) 
    {
        return E_FAIL;
    }
    return S_OK;
}

// Fill the output pin with MPEG-TS packets
HRESULT Pin::FillBuffer(IMediaSample * pSample)
{
    BYTE * pData;

    if (m_input.is_open())
    {
        // Access the sample's data buffer
        pSample->GetPointer(&pData);
        // Read the sample from the file straight into the buffer
        if (m_input.read((char*)pData, FILTER_SAMPLE_SIZE))
        {
            pSample->SetActualDataLength((long)m_input.gcount());
            REFERENCE_TIME rtStart = m_sample * FILTER_SAMPLE_SIZE;
            REFERENCE_TIME rtStop  = rtStart + FILTER_SAMPLE_SIZE;
            pSample->SetTime(&rtStart, &rtStop);
            pSample->SetSyncPoint(TRUE);
            m_sample++;
            Sleep(200);
            return S_OK;
        }
        m_input.clear();
        m_input.seekg(0);
    }
    return S_FALSE;
}
