#ifndef __FILTER_INC
#define __FILTER_INC

/* includes */

#include <windows.h>
#include <initguid.h>
#include <streams.h>
#include <fstream>


using namespace std;


/* defines */

// Defines the name used to register the filter
#define FILTER_NAME     L"MPEG-TS Push Filter"
// Define the default input filename
#define FILTER_FILE     ".\\input.ts"


/* macros */

// {9A80E195-3BBA-4821-B18B-21BB496F80F8}
DEFINE_GUID(CLSID_TSPushFilter, 0x9a80e195, 0x3bba, 0x4821, 0xb1, 0x8b, 0x21, 0xbb, 0x49, 0x6f, 0x80, 0xf8);


/* typedefs */

// Output PIN
class Pin : public CSourceStream
{
    public:
        Pin(LPTSTR pName, HRESULT * pHresult, CSource * pFilter) :
            CSourceStream(pName, pHresult, pFilter, L"Out"),
                m_sample(0)
        {
            m_input.open(FILTER_FILE, ios::binary);
        }

        ~Pin()
        {
            m_input.close();
        }

        // --- CSourceStream methods ---
        HRESULT GetMediaType(CMediaType * pMediaType);
        HRESULT DecideBufferSize(IMemAllocator * pAlloc, ALLOCATOR_PROPERTIES * pRequest);
        HRESULT FillBuffer(IMediaSample * pSample);

    private:
        DWORD m_sample;     // Current sample number
        ifstream m_input;   // Input file stream
};

// Push Filter
class Filter : public CSource
{
    private:
        Filter(LPTSTR pName, LPUNKNOWN pUnknown, HRESULT * pHresult) : 
            CSource(pName, pUnknown, CLSID_TSPushFilter)
        {
            // Allocate the output pin
            if (NULL == (m_pin = new Pin(pName, pHresult, this)))
            {
                *pHresult = E_OUTOFMEMORY;
            }
            else
            {
                *pHresult = S_OK;
            }
        }

        ~Filter()
        {
            delete m_pin;
        }

    public:
        // COM standard filter allocation method
        static CUnknown * WINAPI CreateInstance(LPUNKNOWN pUnknown, HRESULT * pHresult)
        {
            Filter * pFilter;

            // Allocate the filter
            if (NULL == (pFilter = new Filter(FILTER_NAME, pUnknown, pHresult))) 
            {
                *pHresult = E_OUTOFMEMORY;
            }
            else
            {
                *pHresult = S_OK;
            }
            return pFilter;
        }

    private:
        Pin * m_pin;    // Output pin
};

#endif
